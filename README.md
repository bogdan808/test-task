# Тестовое задание 

Спасибо большое за тестовое задание. Оно оказалось интересным и познавательным.
Реализовал с помощью flex + grid.

Постарался вынести значения css свойств в переменные.

Прошу не судить строго нейминг некоторых компонентов. Т.к. проект из серии "проект в вакууме".

## Установка

- Node 12.18.3

```bash
git clone git@bitbucket.org:bogdan808/test-task.git
npm i
npm start          # запуск сервера разработки
```

В проекте присутствуют дефолтные заготовки блоков.
Мои лежат по следующим путям.
## Мои блоки

```bash 
src/       
  blocks/page-index              
  blocks/freeze              
  blocks/personal-data              
  blocks/warmly              
  blocks/card              
  blocks/square          
  blocks/header              
  blocks/hot
```
